import React, {useContext, useEffect, useState} from 'react';
import {View} from 'react-native';
import {Button, Center, Divider, Box, Text, VStack, HStack} from 'native-base';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {AuthenticationContext} from '../Context/index';

const Home = ({navigation}) => {
  const {signOut} = useContext(AuthenticationContext);
  const [user, setUser] = useState({});

  const _userLogout = async () => {
    await signOut();
  };

  const getUser = async () => {
    const userValue = await AsyncStorage.getItem('user');
    await setUser(JSON.parse(userValue));
    console.log('auth home', userValue);
  };

  useEffect(() => {
    getUser();
  }, []);
  return (
    <Box flex={1} bg="#fff" alignItems="center">
      <Text>Hi, {user.name} </Text>
      <VStack space="4" alignItems="center">
        <HStack alignItems="center">
          <Button onPress={() => navigation.navigate('Details')}>
            Detail Screen
          </Button>
        </HStack>
        <HStack alignItems="center">
          <Button onPress={() => navigation.navigate('Lazyload')}>
            Lazy Load
          </Button>
        </HStack>
        <HStack alignItems="center">
          <Button onPress={_userLogout}>Logout</Button>
        </HStack>
        <HStack alignItems="center">
          <Button onPress={() => navigation.navigate('GeolocationScreen')}>
            Geolocation
          </Button>
        </HStack>
      </VStack>
    </Box>
  );
};

export default Home;
