import React, {useState} from 'react';
import {
  HStack,
  Box,
  Heading,
  VStack,
  Text,
  Divider,
  Center,
  Button,
} from 'native-base';
import ModalCustom from '../Components/Modal';

const ListComponents = ({navigation}) => {
  const [showModal, setShowModal] = useState(false);

  const handleOpenModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  return (
    <Box
      flex={1}
      rounded="lg"
      overflow="hidden"
      borderColor="coolGray.200"
      borderWidth="1"
      _dark={{
        borderColor: 'coolGray.600',
        backgroundColor: 'gray.700',
      }}
      _web={{
        shadow: 2,
        borderWidth: 0,
      }}
      _light={{
        backgroundColor: 'gray.50',
      }}>
      <Heading mb="3" textAlign="center">
        Native Base v 3.2.2
      </Heading>
      <VStack space="4" alignItems="center">
        <HStack alignItems="center">
          <Button onPress={() => navigation.navigate('ListCard')}>Card</Button>
        </HStack>
        <HStack alignItems="center">
          <Button onPress={handleOpenModal}>Modal</Button>
        </HStack>
      </VStack>

      {/* MODAL COMPONENT */}
      <ModalCustom
        isOpen={showModal}
        onClose={handleCloseModal}
        header="Order"
        body={
          <VStack space={3}>
            <HStack alignItems="center" justifyContent="space-between">
              <Text fontWeight="medium">Sub Total</Text>
              <Text color="blueGray.400">$298.77</Text>
            </HStack>
            <HStack alignItems="center" justifyContent="space-between">
              <Text fontWeight="medium">Tax</Text>
              <Text color="blueGray.400">$38.84</Text>
            </HStack>
            <HStack alignItems="center" justifyContent="space-between">
              <Text fontWeight="medium">Total Amount</Text>
              <Text color="green.500">$337.61</Text>
            </HStack>
          </VStack>
        }
        footer={
          <Button flex="1" onPress={handleCloseModal}>
            Close
          </Button>
        }
      />
    </Box>
  );
};

export default ListComponents;
