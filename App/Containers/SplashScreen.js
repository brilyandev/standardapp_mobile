import React, {useEffect, useState} from 'react';
import {Spinner, HStack, Center} from 'native-base';
import {NativeBaseProvider, Text, Box} from 'native-base';
import AsyncStorage from '@react-native-async-storage/async-storage';

const SplashScreen = ({navigation}) => {
  // const [authLoaded, setAuthLoaded] = useState(true);
  // const [userToken, setUserToken] = useState(null);
  const getToken = async () => {
    console.log('splash gettoken');
    let tokenvalue = await AsyncStorage.getItem('token');
    // setUserToken(tokenvalue);
    // setIsloading(false);

    if (tokenvalue !== null) {
      navigation.navigate('LoggedInStack');
    } else {
      navigation.navigate('NotLoggedInStack');
    }
  };

  useEffect(() => {
    getToken();
    console.log('splash');
  }, []);
  return (
    <NativeBaseProvider>
      <Center flex={1} px="3">
        <HStack space={2} alignItems="center">
          <Spinner accessibilityLabel="Loading posts" size="lg" />
        </HStack>
      </Center>
    </NativeBaseProvider>
  );
};

export default SplashScreen;
