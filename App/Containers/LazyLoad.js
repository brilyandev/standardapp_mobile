import React, {useState, useEffect} from 'react';
import {Box, FlatList, Avatar, HStack, VStack, Text} from 'native-base';

import usePhotos from '../Hooks/Photo/usePhotos';

const LazyLoad = () => {
  const {photos} = usePhotos();

  return (
    <Box
      w={{
        base: '100%',
        md: '25%',
      }}>
      <FlatList
        onEndReachedThreshold={0.7}
        data={photos}
        renderItem={({item}) => (
          <Box
            borderBottomWidth="1"
            _dark={{
              borderColor: 'gray.600',
            }}
            borderColor="coolGray.200"
            pl="4"
            pr="5"
            py="2">
            <HStack space={3}>
              <Avatar
                size="48px"
                source={{
                  uri: item.thumbnailUrl,
                }}
              />
              <VStack>
                <Text
                  _dark={{
                    color: 'warmGray.50',
                  }}
                  color="coolGray.800"
                  bold>
                  {item.title}
                </Text>
                <Text
                  color="coolGray.600"
                  _dark={{
                    color: 'warmGray.200',
                  }}>
                  {item.url}
                </Text>
              </VStack>
            </HStack>
          </Box>
        )}
        keyExtractor={item => item.id}
      />
    </Box>
  );
};

export default LazyLoad;
