import React, {useState, useContext} from 'react';
import {
  FormControl,
  Input,
  Stack,
  WarningOutlineIcon,
  Center,
  Button,
  Text,
} from 'native-base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {AuthenticationContext} from '../Context/index';

// import apiauth from '../Services/Auth';

const LoginScreen = ({navigation}) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const {signIn, errormsg} = useContext(AuthenticationContext);

  const _userLogin = async () => {
    // console.log('press');
    let datasend = {username, password, app: 'gamesApp'};

    signIn(datasend);
  };

  const _userSignUp = async () => {
    let tokenvalue = await AsyncStorage.getItem('token');
    console.log('getitem', tokenvalue);
  };

  return (
    <Center flex={1} px="3">
      <FormControl>
        <Stack mx="4" space={2}>
          <>
            <FormControl.Label>Username</FormControl.Label>
            <Input
              placeholder="Username"
              value={username}
              onChangeText={e => setUsername(e)}
            />
          </>
          <>
            <FormControl.Label>Password</FormControl.Label>
            <Input
              type="password"
              placeholder="password"
              value={password}
              onChangeText={e => setPassword(e)}
            />
          </>
        </Stack>

        <Stack mx="4" space={2} m={2}>
          <Button onPress={_userLogin}>Login</Button>
        </Stack>
        <Stack mx="4" space={2}>
          <Button onPress={_userSignUp}>Sign Up</Button>
        </Stack>
        <Stack mx="4" space={2}>
          <Center>
            <Text color="danger.600">{errormsg}</Text>
          </Center>
        </Stack>
      </FormControl>
    </Center>
  );
};

export default LoginScreen;
