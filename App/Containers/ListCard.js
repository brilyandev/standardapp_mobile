import React from 'react';
import Card from '../Components/Card';
import Card2 from '../Components/Card2';
import CustomCard from '../Components/CustomCard';
import {Center, VStack, HStack, Text} from 'native-base';

const ListCard = () => {
  return (
    <Center flex={1} px="3">
      <VStack space={2}>
        <HStack justifyContent="space-between">
          <CustomCard header="Header" content="Hello World" />
          <CustomCard header="Header" content="Hello World" />
          <CustomCard header="Header" content="Hello World" />
        </HStack>
        {/* <CustomCard header="Header" content="Hello World" /> */}
        <Card
          header="Header"
          subheader="Sub Header"
          content="NativeBase is a free and open source framework that enable developers
          to build high-quality mobile apps using React Native iOS and Android
          apps with a fusion of ES6"
          smallfooter="Small Footer"
        />
        <Card2
          header="NativeBase"
          content="NativeBase is a free and open source framework that enable developers
          to build high-quality mobile apps using React Native iOS and Android
          apps with a fusion of ES6."
          footer="GeekyAnts"
        />
      </VStack>
    </Center>
  );
};

export default ListCard;
