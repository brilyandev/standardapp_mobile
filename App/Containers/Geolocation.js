import React, {useState, useEffect} from 'react';
import {PermissionsAndroid} from 'react-native';
import {Text, VStack} from 'native-base';
import Geolocation from 'react-native-geolocation-service';
import Geocoder from 'react-native-geocoding';

const GeolocationScreen = () => {
  const [currentLongitude, setCurrentLongitude] = useState('...');
  const [currentLatitude, setCurrentLatitude] = useState('...');
  const [locationStatus, setLocationStatus] = useState('');
  const [address, setAddress] = useState('');

  const getOneTimeLocation = () => {
    console.log('function one time location');
    Geolocation.getCurrentPosition(
      //Will give you the current location
      position => {
        setLocationStatus(' ');

        //getting the Longitude from the location json
        const currentLongitude = JSON.stringify(position.coords.longitude);

        //getting the Latitude from the location json
        const currentLatitude = JSON.stringify(position.coords.latitude);

        //Setting Longitude state
        setCurrentLongitude(currentLongitude);

        //Setting Longitude state
        setCurrentLatitude(currentLatitude);

        Geocoder.init('AIzaSyDpvpbmz1U4BiEbLHK3DD-pXFJkllkYPhI'),
          Geocoder.from(
            position.coords.latitude,
            position.coords.longitude,
          ).then(json => {
            var addressComponent = json.results[0].formatted_address;
            console.log('addressComponent', addressComponent);
            setAddress(addressComponent);
          });
      },
      error => {
        setLocationStatus(error.message);
      },
      {
        enableHighAccuracy: false,
        timeout: 30000,
        maximumAge: 1000,
      },
    );
  };

  const subscribeLocationLocation = () => {
    console.log('subcribe location');
    watchID = Geolocation.watchPosition(
      position => {
        //Will give you the location on location change

        setLocationStatus(' ');
        console.log('position', position);

        //getting the Longitude from the location json
        const currentLongitude = JSON.stringify(position.coords.longitude);

        //getting the Latitude from the location json
        const currentLatitude = JSON.stringify(position.coords.latitude);

        //Setting Longitude state
        setCurrentLongitude(currentLongitude);

        //Setting Latitude state
        setCurrentLatitude(currentLatitude);
      },
      error => {
        setLocationStatus(error.message);
      },
      {
        enableHighAccuracy: false,
        maximumAge: 1000,
      },
    );
  };

  useEffect(() => {
    const requestLocationPermission = async () => {
      if (Platform.OS === 'ios') {
        getOneTimeLocation();
        subscribeLocationLocation();
      } else {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              title: 'Location Access Required',
              message: 'This App needs to Access your location',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            //To Check, If Permission is granted
            getOneTimeLocation();
            subscribeLocationLocation();
          } else {
            setLocationStatus('Permission Denied');
          }
        } catch (err) {
          console.warn('err', err);
        }
      }
    };
    requestLocationPermission();
    return () => {
      Geolocation.clearWatch(watchID);
    };
  }, []);
  return (
    <VStack space={4} alignItems="center">
      <Text>{address}</Text>
    </VStack>
  );
};

export default GeolocationScreen;
