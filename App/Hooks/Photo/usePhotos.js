import {useEffect, useContext} from 'react';
import {PhotosContext} from '../../Context/index';

export default function usePhotos() {
  const {getPhotos, photos} = useContext(PhotosContext);

  useEffect(() => {
    getPhotos();
  }, []);

  return {photos, getPhotos};
}
