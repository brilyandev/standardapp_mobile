import React from 'react';
import {VStack, Box, Divider} from 'native-base';

export default function (props) {
  return (
    <Box
      //   maxWidth={70}
      border="1"
      borderRadius="md"
      borderColor="coolGray.200"
      _dark={{
        borderColor: 'coolGray.600',
        backgroundColor: 'gray.700',
      }}
      _light={{
        backgroundColor: 'gray.50',
      }}>
      <VStack divider={<Divider bg="primary.500" />}>
        <Box p="4" bg="primary.500">
          {props.header}
        </Box>
        <Box p="4">{props.content}</Box>
        {/* <Box px="4" pb="4">
          {props.footer}
        </Box> */}
      </VStack>
    </Box>
  );
}
