import React from 'react';
import {Modal} from 'native-base';

const ModalComponent = props => {
  return (
    <Modal isOpen={props.isOpen} onClose={props.onClose}>
      <Modal.Content maxWidth="350">
        <Modal.CloseButton />
        <Modal.Header>{props.header}</Modal.Header>
        <Modal.Body>{props.body}</Modal.Body>
        <Modal.Footer>{props.footer}</Modal.Footer>
      </Modal.Content>
    </Modal>
  );
};

export default ModalComponent;
