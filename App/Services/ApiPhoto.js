import axios from 'axios';
import StaticVar from '../Config/StaticVar';

// ===> api create
const api = axios.create({
  baseURL: StaticVar.Example_Url,
  // timeout: 10000,
  headers: {},
});

const getPhotos = () => api.get('/photos');

export const apis = {
  getPhotos,
};

export default apis;
