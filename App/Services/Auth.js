import axios from 'axios';
import StaticVar from '../Config/StaticVar';
import AsyncStorage from '@react-native-async-storage/async-storage';

// ===> api create
const api = axios.create({
  baseURL: StaticVar.Auth_Url,
  // timeout: 10000,
  headers: {},
});

// ===> api interceptors
api.interceptors.request.use(
  function (config) {
    // set headers after authentication
    config.headers['x-access-token'] = AsyncStorage.getItem('token');
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  },
);

// ===> api list function request
const authRequest = data => api.post('/auth/login', data);

export const apis = {
  authRequest,
};

export default apis;
