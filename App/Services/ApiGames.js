import axios from 'axios';
import StaticVar from '../Config/StaticVar';
import AsyncStorage from '@react-native-async-storage/async-storage';

// ===> api create
const api = axios.create({
  baseURL: StaticVar.Base_Apigames,
  // timeout: 10000,
  headers: {},
});

// ===> api interceptors
// api.interceptors.request.use(
//   function (config) {
//     // set headers after authentication
//     config.headers['x-access-token'] = AsyncStorage.getItem('token');
//     return config;
//   },
//   function (error) {
//     // Do something with request error
//     return Promise.reject(error);
//   },
// );

const getContingents = data => api.get('/contingents', data);
const getAthletes = query => api.get(`/athletes/all${query}`);
const getParticipants = query => api.get(`/athletes${query}`);
const getEvent = data => api.get('/events', data);

export const apis = {
  getContingents,
  getAthletes,
  getParticipants,
  getEvent,
};

export default apis;
