import axios from 'axios';
import StaticVar from '../Config/StaticVar';
import AsyncStorage from '@react-native-async-storage/async-storage';

// ===> api create
const api = axios.create({
  baseURL: StaticVar.Wa_Url,
  // timeout: 10000,
  headers: {},
});

// ===> api interceptors
api.interceptors.request.use(
  function (config) {
    // set headers after authentication
    config.headers['x-access-token'] = AsyncStorage.getItem('token');
    // console.log('localStorage.getItem("token")', AsyncStorage.getItem('token'));
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  },
);

const postDataToWaBlast = data => api.post('/postDataToWaBlast', data);

export const apis = {
  postDataToWaBlast,
};

export default apis;
