import axios from 'axios';
import StaticVar from '../Config/StaticVar';
import AsyncStorage from '@react-native-async-storage/async-storage';

// ===> api create
const api = axios.create({
  baseURL: StaticVar.Schedule_url,
  // timeout: 10000,
  headers: {},
});

// ===> api interceptors
api.interceptors.request.use(
  function (config) {
    // set headers after authentication
    config.headers['x-access-token'] = AsyncStorage.getItem('token');
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  },
);

const getSchedules = data => api.get(`/schedules`, data);
const getDetailSchedule = id => api.get('/schedules/' + id);
const getMedalsContingent = data => api.get(`/medals/classement`, data);
const getMedalsDashboard = data => api.get(`/medals/dashboard`, data);
const getMedalsAchievers = data => api.get(`/medals/byparticipant`, data);
const getMedalist = data => api.get(`/medals`, data);
const getMedalsSport = data => api.get(`/medals/classementbysport`, data);
const getEventGrouping = data => api.get(`/public/eventgrouping`, data);
const postGroupClassment = data => api.post(`/schedules/groupClassmen`, data);
const getBracket = query => api.get(`/bagan${query}`);

export const apis = {
  getSchedules,
  getDetailSchedule,
  getMedalsContingent,
  getMedalsDashboard,
  getMedalsAchievers,
  getMedalist,
  getMedalsSport,
  getEventGrouping,
  postGroupClassment,
  getBracket,
};

export default apis;
