import React, {useState, useEffect, useRef} from 'react';
import {NativeBaseProvider, Text, Box} from 'native-base';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import AsyncStorage from '@react-native-async-storage/async-storage';

import HomeScreen from '../Containers/Home';
import DetailsScreen from '../Containers/Details';
import LazyLoadScreen from '../Containers/LazyLoad';
import NotificationsScreen from '../Containers/NotificationScreen';
import ListComponentScreen from '../Containers/ListComponents';
import CardScreen from '../Containers/ListCard';
import Geolocation from '../Containers/Geolocation';

const Drawer = createDrawerNavigator();

function MenuDrawer() {
  return (
    <Drawer.Navigator initialRouteName="Home">
      <Drawer.Screen name="Home" component={HomeScreen} />
      <Drawer.Screen name="Components" component={ListComponentScreen} />
    </Drawer.Navigator>
  );
}

const HomeStack = createNativeStackNavigator();
export default function HomeStackScreen() {
  const [userToken, setUserToken] = useState(null);

  const getToken = async () => {
    let tokenvalue = await AsyncStorage.getItem('token');
    setUserToken(tokenvalue);
  };

  useEffect(() => {
    getToken();
  }, []);
  return (
    <HomeStack.Navigator initialRouteName="Home">
      <HomeStack.Screen
        name="Home"
        component={MenuDrawer}
        options={{headerShown: false}}
      />
      <HomeStack.Screen name="Details" component={DetailsScreen} />
      <HomeStack.Screen name="Lazyload" component={LazyLoadScreen} />
      <HomeStack.Screen name="ListCard" component={CardScreen} />
      <HomeStack.Screen name="GeolocationScreen" component={Geolocation} />
    </HomeStack.Navigator>
  );
}
